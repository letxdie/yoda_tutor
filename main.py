import json
import time
import configs
import telebot

bot = telebot.TeleBot(configs.Token)



def change_state(new_state, message):
    with open('user.json', 'r', encoding='utf-8') as fh:
        user = json.load(fh)
        for i in user:
            if i['id'] == message.chat.id:
                i['state'] = new_state
    with open('user.json', 'w', encoding='utf-8') as fh:
        json.dump(user, fh)
        fh.close()


def load_state(message):
    with open('user.json', 'r', encoding='utf-8') as fh:
        user = json.load(fh)
        for i in user:
            if i['id'] == message.chat.id:
                state = i['state']
                return state


subjects = [[1, [1, 2, 3]], [2, [1, 2, 3]], [3, [1, 2, 3]]]


@bot.message_handler(commands=['start'])
def start(message):
    change_state(-1, message)
    if message.chat.username and message.chat.id:
        user_dict = {
            "choice": [
                "0",
                "0"
            ],
            "count": "",
            "correct": "",
            "state": -1,
            "username": message.chat.username,
            "id": message.chat.id,
            "subjects": {
                "1": {
                    "1": "0",
                    "2": "0",
                    "3": "0"
                },
                "2": {
                    "1": "0",
                    "2": "0",
                    "3": "0"
                },
                "3": {
                    "1": "0",
                    "2": "0",
                    "3": "0"
                }
            }
        }
        with open('user.json', 'r', encoding='utf-8') as fh:
            user = json.load(fh)
            already_exists = False
            for i in user:
                if user_dict['id'] == i['id']:
                    bot.send_message(message.chat.id, "Рад видеть снова вас я")
                    already_exists = True
            if not already_exists:
                user.append(user_dict)
                fh.close()
                with open('user.json', 'w', encoding='utf-8') as fh:
                    json.dump(user, fh)
                    fh.close()
                bot.send_message(message.chat.id, "Привет, падаван мой юный ")
                bot.send_message(message.chat.id, "Формулы буду помогать учить тебе я")


    else:
        bot.send_message(message.chat.id, "Профиль настроен ваш неправильно")


@bot.message_handler(commands=['plan'])
def plan(message):
    change_state(-1, message)
    fp = open(f'images/3/3/answer.jpg', 'rb')
    bot.send_photo(message.chat.id, fp)
    change_state(-1, message)


@bot.message_handler(commands=['creator'])
def me(message):
    change_state(-1,message)
    bot.send_message(message.chat.id,'Created by Evgeny Gutin \n VK: https://vk.com/jenia44\n INST: crevvofficial_')


@bot.message_handler(commands=['test'])
def list(message):
    bot.send_message(message.chat.id, """Просто отправь мне раздела и темы НОМЕРА нужные ты 
    
    <b>1 - Математика</b>
        1 - Тригонометрия
        2 - Логарифмы
        3 - Пределы
    <b>2 - Физика</b>
        1 - Геометрическая оптика
        2 - Основы динамики
        3 - Квантовая физика
    <b>3 - Информатика</b>
        1 - Логические операции
        2 - Coming soon..
        3 - Coming soon.. \n Пример ввода "1 1"
    """, parse_mode='html')
    change_state(1, message)


@bot.message_handler(commands=['study'])
def study(message):
    bot.send_message(message.chat.id, """Просто отправь мне раздела и темы НОМЕРА нужные ты 
    <b>1 - Математика</b>
        1 - Тригонометрия
        2 - Логарифмы
        3 - Пределы
    <b>2 - Физика</b>
        1 - Геометрическая оптика
        2 - Основы динамики
        3 - Квантовая физика
    <b>3 - Информатика</b>
        1 - Логические операции
        2 - Coming soon..
        3 - Coming soon.. \n Пример ввода "1 1"
    """, parse_mode='html')
    change_state(0, message)


@bot.message_handler(commands=['stat'])
def stat(message):
    mass = []
    output = ''
    mass_y =['Тригонометрия','Логарифмы','Пределы','Геометрическая оптика','Основы динамики','Квантовая физика','Логические операции','Coming soon..','Coming soon..']
    with open('user.json', 'r', encoding='utf-8') as fh:
        user = json.load(fh)
        for i in user:
            if i['id'] == message.chat.id:
                list = i['subjects']
                for k in range(1, 4):
                    g = str(k)
                    list_k = list[g]
                    for n in range(1, 4):
                        q = str(n)
                        list_q = list_k[q]
                        mass.append(list_q)
        for i in range(len(mass)):
            output += mass_y[i]+'-<b>'+str(mass[i]) + '%</b>\n'
    change_state(-1,message)
    bot.send_message(message.chat.id,output,parse_mode='html')




@bot.message_handler(content_types=['text'])
def matrix(message):
    state = load_state(message)
    if state == -1:
        bot.send_message(message.chat.id, 'Извини, не люблю говорить не по делу я')
    if state == 0:
        return show(message)
    if state == 1:
        return subject_theme(message)
    if state == 2:
        return yes_or_no(message)
    if state == 3:
        return test(message)

def show(message):
    try:
        current_subject, current_theme = message.text.split()
        for subject in subjects:
            if current_subject == str(subject[0]):

                for theme in subject[1]:  # subject[1] - список тем
                    if current_theme == str(theme):
                        choiz = (current_subject, current_theme)
                        print(choiz)
                        fp = open(f'images/{choiz[0]}/{choiz[1]}/answer.jpg', 'rb')
                        bot.send_photo(message.chat.id, fp)
                        change_state(-1, message)
                        return None
        bot.send_message(message.chat.id, "Ввел неправильно ты.\n Пример ввода: 1 1")
        return study(message)
    except ValueError:
        bot.send_message(message.chat.id, "Недопстумый ввода формат у тебя")
        return study(message)


def load_choice(message,choice):
    with open('user.json', 'r', encoding='utf-8') as fh:
        user = json.load(fh)
        for i in user:
            if i['id'] == message.chat.id:
                i['choice'] = choice
    with open('user.json', 'w', encoding='utf-8') as fh:
        json.dump(user, fh)
        fh.close()


def choice(message):
    with open('user.json', 'r', encoding='utf-8') as fh:
        user = json.load(fh)
        for i in user:
            if i['id'] == message.chat.id:
                choice = i['choice']
                return choice

def subject_theme(message):
    try:
        current_subject, current_theme = message.text.split()
        for subject in subjects:
            if current_subject == str(subject[0]):

                for theme in subject[1]:  # subject[1] - список тем
                    if current_theme == str(theme):
                        choice = (current_subject, current_theme)
                        load_choice(message, choice)
                        print(choice)
                        return test_or_edu(choice, message)
        bot.send_message(message.chat.id, "Ввел неправильно ты")
        return list(message)
    except ValueError:
        bot.send_message(message.chat.id, "Недопстумый ввода формат у тебя")
        return list(message)


def test_or_edu(choice, message):
    subject, theme = choice
    with open('user.json', 'r', encoding='utf-8') as fh:
        user = json.load(fh)
        for i in user:
            if i['id'] == message.chat.id:
                if i['subjects'][subject][theme] == "0":
                    bot.send_message(message.chat.id, 'Не проходил ты еще тесты по теме этой')
                    bot.send_message(message.chat.id, 'Начнем?')
                    change_state(2, message)
                    return None
                bot.send_message(message.chat.id, 'Начнем?')
                change_state(2, message)


def yes_or_no(message):
    if message.text == 'да' or message.text == 'Да':
        change_state(3, message)
        return matrix(message)
    elif message.text == 'нет' or message.text == 'Нет':
        bot.send_message(message.chat.id, 'Очень жаль')
        change_state(-1, message)
        return None
    else:
        bot.send_message(message.chat.id, 'так да или нет?')


def load_correct(message, correct='+'):
    with open('user.json', 'r', encoding='utf-8') as fh:
        user = json.load(fh)
        for i in user:
            if i['id'] == message.chat.id:
                i['correct'] += correct
    with open('user.json', 'w', encoding='utf-8') as fh:
        json.dump(user, fh)
        fh.close()


def correct(message):
    with open('user.json', 'r', encoding='utf-8') as fh:
        user = json.load(fh)
        for i in user:
            if i['id'] == message.chat.id:
                correct = i['correct']
                return correct


def zero_correct(message):
    with open('user.json', 'r', encoding='utf-8') as fh:
        user = json.load(fh)
        for i in user:
            if i['id'] == message.chat.id:
                i['correct'] = ''
    with open('user.json', 'w', encoding='utf-8') as fh:
        json.dump(user, fh)
        fh.close()


def load_count(message, count='!'):
    with open('user.json', 'r', encoding='utf-8') as fh:
        user = json.load(fh)
        for i in user:
            if i['id'] == message.chat.id:
                i['count'] += count
    with open('user.json', 'w', encoding='utf-8') as fh:
        json.dump(user, fh)
        fh.close()


def cunt(message):
    with open('user.json', 'r', encoding='utf-8') as fh:
        user = json.load(fh)
        for i in user:
            if i['id'] == message.chat.id:
                count = i['count']
                return len(count)


def zero_count(message):
    with open('user.json', 'r', encoding='utf-8') as fh:
        user = json.load(fh)
        for i in user:
            if i['id'] == message.chat.id:
                i['count'] = ''
    with open('user.json', 'w', encoding='utf-8') as fh:
        json.dump(user, fh)
        fh.close()


def test(message):
    n_list = [0]
    a_list = [0]
    n_choice = choice(message)
    s_choice = n_choice[0] + n_choice[1]
    with open('ans.json', 'r', encoding='utf-8') as fh:
        user = json.load(fh)
        for i in user:
            if i[0] == s_choice:
                x = i[1]
                for n in x:
                    n_list.append(n)
                    a_list.append(x[n])
                    n_list.append('n')

    count = cunt(message)
    if count == 5:
        if message.text == a_list[count]:
            load_correct(message)
            print(correct(message))
        load_count(message)
        print(count)
        x = correct(message)
        x = len(x)
        bot.send_message(message.chat.id, 'Так, подсчитаю правильные ответы сейчас я')
        time.sleep(3)
        bot.send_message(message.chat.id, f'{x} Правильных ответов')
        correct_counter(message, x,s_choice)
        zero_correct(message)
        zero_count(message)
        change_state(-1,message)
        return None

    bot.send_message(message.chat.id, f'Введите ответ на вопрос №{count+1}')
    fp = open(f'images/{n_choice[0]}/{n_choice[1]}/{str(count+1)}.jpg', 'rb')
    bot.send_photo(message.chat.id, fp)
    if message.text == a_list[count]:
        load_correct(message)
        print(correct(message))
    load_count(message)
    print(count)


def correct_counter(message, count, choice):
    subject = choice[0]
    theme = choice[1]
    count = count/5 * 100
    print(count)
    with open('user.json', 'r', encoding='utf-8') as fh:
        user = json.load(fh)
        for i in user:
            if i['id'] == message.chat.id:
                i['subjects'][subject][theme] = count
    with open('user.json', 'w', encoding='utf-8') as fh:
        json.dump(user, fh)
        fh.close()


if __name__ == '__main__':
    bot.polling(none_stop=True)
